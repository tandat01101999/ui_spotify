import 'package:flutter/material.dart';
import 'package:test_1/common/custom_incaditor.dart';
import 'package:test_1/common/list_view_item.dart';
import 'package:test_1/core/app_font_style.dart';

import '../../core/app_color.dart';

// ignore: must_be_immutable
class TabBarHomeWidget extends StatefulWidget {
  TabBarHomeWidget({Key? key,required this.item}) : super(key: key);
  List item;

  @override
  State<TabBarHomeWidget> createState() => _TabBarHomeWidgetState();
}

class _TabBarHomeWidgetState extends State<TabBarHomeWidget> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    TabController tabController = TabController(length: 4, vsync: this);
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 32),
            child: TabBar(
              padding: EdgeInsets.zero,
              labelColor: Colors.white,
              labelPadding: EdgeInsets.zero,
              unselectedLabelColor: Colors.white38,
              controller: tabController,
              indicatorColor: Colors.red,
              indicatorPadding: const EdgeInsets.only(bottom: 7),
              indicator: const CustomTabIndicator(),
              tabs: const [
                Tab(
                  child: Text(
                    "Artists",
                    style: FontTextStyle.fontTextTabBar,
                  ),
                ),
                Tab(
                  child: Text(
                    "Album",
                    style: FontTextStyle.fontTextTabBar,
                  ),
                ),
                Tab(
                  child: Text(
                    "Podcast",
                    style: FontTextStyle.fontTextTabBar,
                  ),
                ),
                Tab(
                  child: Text(
                    "Genre",
                    style: FontTextStyle.fontTextTabBar,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 16,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: <Color>[
                  AppColor.colorBackground.withOpacity(0.08),
                  AppColor.colorWhiteBorder.withOpacity(0.4)
                ],
              ),
            ),
          ),
          SizedBox(
            width: double.maxFinite,
            height: 400,
            child: TabBarView(
              controller: tabController,
              children: [
                Padding(
                  padding:
                  const EdgeInsets.only(left: 32, right: 32, top: 9),
                  child: ListViewItem(
                    image: widget.item,
                    icon: "assets/image/ic_arrow.png",
                    isCheckRadius: false,
                  ),
                ),
                const Center(
                  child: Text(
                    "Album",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                const Center(
                  child: Text(
                    "Podcast",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                const Center(
                  child: Text(
                    "Genre",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
