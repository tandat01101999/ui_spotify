import 'package:flutter/material.dart';

import '../../core/app_font_style.dart';

// ignore: must_be_immutable
class WidgetHomeListView extends StatelessWidget {
  WidgetHomeListView({Key? key,required this.item}) : super(key: key);
  List item;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      margin: const EdgeInsets.only(left: 32),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: item.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.fromLTRB(0, 12, 16, 0),
            child: InkWell(
              onTap: () {},
              child: Column(
                children: [
                  Container(
                    height: 128,
                    width: 150,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image:
                            AssetImage(item[index]["image"]),
                            fit: BoxFit.cover)),
                  ),
                  const SizedBox(height: 10),
                  SizedBox(
                    width: 130,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          item[index]["name"],
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: FontTextStyle.fontTextWhite.copyWith(fontSize: 14),
                        ),
                        Text(
                          item[index]['description'],
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: FontTextStyle.fontTextWhite38.copyWith(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
