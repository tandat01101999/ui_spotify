import 'dart:ui';
import 'package:flutter/material.dart';
class FontTextStyle {
  static const appBarText = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );
  static const fontTextWhite = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w600,
    color: Colors.white,
  );
  static const fontTextWhite38 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w400, color: Colors.white38);

  static const fontTextTabBar = TextStyle(
      fontSize: 20, fontWeight: FontWeight.bold);
}
