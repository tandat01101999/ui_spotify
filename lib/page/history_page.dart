import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_1/core/app_color.dart';
import 'package:test_1/core/app_font_style.dart';

class HistoryPage extends StatefulWidget {
  const HistoryPage({Key? key}) : super(key: key);

  @override
  State<HistoryPage> createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  StreamController counterController =  StreamController<int>();

  Stream<List> productsStream() async* {
    while (true) {
      await Future.delayed(const Duration(milliseconds: 10));
      final String response = await rootBundle.loadString('assets/json/category.json');
      final data = await json.decode(response);
      List someProduct = data["items"];
      yield someProduct;
    }
  }


  @override
  void dispose() {
    counterController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.colorBackgroundNew,
      appBar: AppBar(
        elevation: 10,
        actions: [Image.asset("assets/image/ic_more_app_bar.png")],
        backgroundColor: AppColor.colorBackgroundNew,
        title: const Text(
          "History",
          style: FontTextStyle.appBarText,
        ),
        centerTitle: true,
      ),
      body:SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 16,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: <Color>[
                        AppColor.colorBackground.withOpacity(0.08),
                        AppColor.colorWhiteBorder.withOpacity(0.4)
                      ])),
            ),
            const SizedBox(height: 20,),
            const Padding(
              padding:EdgeInsets.only(left: 32),
              child: Text("Today",style: FontTextStyle.appBarText,),
            ),
            StreamBuilder(
              stream: productsStream(),
              builder: (context, snapshot) {
               if(snapshot.hasData){
                 return Column(
                   children: [
                     Padding(
                       padding:const EdgeInsets.only(left: 32,top: 16),
                       child: _itemHistory(snapshot.data![0]["image"],snapshot.data![0]["name"],snapshot.data![0]["description"],80,80),
                     ),
                     Padding(
                       padding:const EdgeInsets.only(left: 48,top: 16),
                       child: _itemHistory(snapshot.data![1]["image"],snapshot.data![1]["name"],snapshot.data![1]["description"],64,64),
                     ),
                     Padding(
                       padding:const EdgeInsets.only(left: 48,top: 16,right: 20),
                       child: Row(
                         children: [
                           ClipRRect(
                             borderRadius: BorderRadius.circular(15),
                             child: SizedBox(
                               height: 64,
                               width: 64,
                               child: Image.asset(snapshot.data![2]["image"], fit: BoxFit.cover),
                             ),
                           ),
                           const SizedBox(width: 10,),
                           Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children:[
                               Text(snapshot.data![2]["name"],style: FontTextStyle.fontTextWhite.copyWith(fontSize: 18),),
                               Text(snapshot.data![2]["description"],style: FontTextStyle.fontTextWhite38,),
                             ],
                           ),
                           const Spacer(),
                           Image.asset("assets/image/ic_more_1.png"),
                         ],
                       ),
                     ),
                     Padding(
                       padding:const EdgeInsets.only(left: 48,top: 16,right: 20),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           Text("See all 28 played!",style: FontTextStyle.fontTextWhite.copyWith(fontSize: 14),),
                           Image.asset("assets/image/ic_more_1.png"),
                         ],
                       ),
                     ),
                     Padding(
                       padding:const EdgeInsets.only(left: 32,right: 25,top: 32),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           const Text("Yesterday",style: FontTextStyle.appBarText),
                           Image.asset("assets/image/ic_arrow.png"),
                         ],
                       ),
                     ),
                     Padding(
                       padding:const EdgeInsets.only(left: 32,top: 16),
                       child: _itemHistory(snapshot.data![2]["image"],snapshot.data![2]["name"],snapshot.data![2]["description"],80,80),
                     ),
                     Padding(
                       padding:const EdgeInsets.only(left: 48,top: 16,right: 20),
                       child: Row(
                         children: [
                           ClipRRect(
                             borderRadius: BorderRadius.circular(15),
                             child: SizedBox(
                               height: 64,
                               width: 64,
                               child: Image.asset(snapshot.data![1]["image"], fit: BoxFit.cover),
                             ),
                           ),
                           const SizedBox(width: 10,),
                           Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: [
                               Text(snapshot.data![1]["name"],style: FontTextStyle.fontTextWhite.copyWith(fontSize: 18)),
                               Text(snapshot.data![1]["description"],style:FontTextStyle.fontTextWhite38,),
                             ],
                           ),
                           const Spacer(),
                           Image.asset("assets/image/ic_more_1.png"),
                         ],
                       ),
                     ),
                     Padding(
                       padding:const EdgeInsets.only(left: 48,top: 16,right: 20),
                       child: Row(
                         children: [
                           ClipRRect(
                             borderRadius: BorderRadius.circular(15),
                             child: SizedBox(
                               height: 64,
                               width: 64,
                               child: Image.asset(snapshot.data![0]["image"], fit: BoxFit.cover),
                             ),
                           ),
                           const SizedBox(width: 10,),
                           Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: [
                               Text(snapshot.data![0]["name"],style: FontTextStyle.fontTextWhite.copyWith(fontSize: 18)),
                               Text(snapshot.data![0]["description"],style: FontTextStyle.fontTextWhite38,),
                             ],
                           ),
                           const Spacer(),
                           Image.asset("assets/image/ic_more_1.png"),
                         ],
                       ),
                     ),
                     Padding(
                       padding:const EdgeInsets.only(left: 48,top: 16,right: 25,bottom: 20),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           Text("See all 28 played!",style: FontTextStyle.fontTextWhite.copyWith(fontSize: 14),),
                           Image.asset("assets/image/ic_arrow.png"),
                         ],
                       ),
                     ),
                   ],
                 );
               }
               return Container();
              }
            )
          ],
        ),
      ),
    );
  }

  Widget _itemHistory(String image ,String title ,String label,double height, double width){
    return Row(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: SizedBox(
            height: height,
            width: width,
            child: Image.asset(image, fit: BoxFit.cover),
          ),
        ),
        const SizedBox(width: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title,style: FontTextStyle.fontTextWhite),
            Text(label,style: FontTextStyle.fontTextWhite38),
          ],
        ),
      ],
    );
  }
}
