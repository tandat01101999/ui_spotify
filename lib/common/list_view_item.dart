import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ListViewItem extends StatelessWidget {
  ListViewItem({Key? key,required this.image,required this.icon,required this.isCheckRadius}) : super(key: key);

  List image;
  String icon;
  bool isCheckRadius;


  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      itemCount: image.length,
      itemBuilder: (BuildContext context, int index) {
        return  Padding(
          padding: const EdgeInsets.only(top: 15),
          child: Row(
            children: [
              isCheckRadius ? ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: SizedBox(
                  height: 64,
                  width: 64,
                  child: Image.asset(image[index]["image"], fit: BoxFit.cover),
                ),
              ):
              ClipOval(
                child: SizedBox.fromSize(
                  size: const Size.fromRadius(35), // Image radius
                  child: Image.asset(image[index]["image"], fit: BoxFit.cover),
                ),
              ),
              const SizedBox(width: 10,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(image[index]["name"],style: const TextStyle(fontSize: 18,fontWeight: FontWeight.w600,color: Colors.white),),
                  Text(image[index]["description"],style: const TextStyle(fontSize: 12,fontWeight: FontWeight.w400,color: Colors.white38),),
                ],
              ),
              const Spacer(),
              Image.asset(icon),
            ],
          ),
        );
      },
    );
  }
}
