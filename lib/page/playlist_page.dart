import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_1/core/app_font_style.dart';
import '../core/app_color.dart';

class PlaylistPage extends StatefulWidget {
  const PlaylistPage({Key? key}) : super(key: key);

  @override
  State<PlaylistPage> createState() => _PlaylistPageState();
}
class _PlaylistPageState extends State<PlaylistPage> {
  List _items = [];
  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/json/play_list.json');
    final data = await json.decode(response);
    setState(() {
      _items = data["items"];
    });
  }

  @override
  void initState() {
    super.initState();
    readJson();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.colorBackgroundNew,
      appBar: AppBar(
        elevation: 10,
        leading: Image.asset("assets/image/ic_search.png"),
        actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.add))],
        backgroundColor: AppColor.colorBackgroundNew,
        title: const Text(
          "Playlist",
          style: FontTextStyle.appBarText,
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Container(
            height: 16,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: <Color>[
                      AppColor.colorBackground.withOpacity(0.08),
                      AppColor.colorWhiteBorder.withOpacity(0.4)
                ])),
          ),
          Expanded(
            child: GridView.builder(
              padding: const EdgeInsets.only(left: 32, right: 32, top: 16),
              itemCount: _items.length,
              itemBuilder: (ctx, i) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image.asset(
                        _items[i]["image"],
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  const SizedBox(height: 0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(_items[i]["name"],
                          style: FontTextStyle.fontTextWhite.copyWith(fontSize: 16)),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(_items[i]["description"],
                          style: FontTextStyle.fontTextWhite.copyWith(fontSize: 14,fontWeight: FontWeight.w400)),
                      const SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                ],
              ),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 0.67,
                  crossAxisSpacing: 30,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
