import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_1/core/app_font_style.dart';

import '../common/list_view_item.dart';
import '../core/app_color.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  List _items = [];

  Future<void> readJson() async {
    final String response =
        await rootBundle.loadString('assets/json/category.json');
    final data = await json.decode(response);
    setState(() {
      _items = data["items"];
    });
  }

  @override
  void initState() {
    super.initState();
    readJson();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.colorBackgroundNew,
      appBar: AppBar(
        elevation: 0,
        actions: [Image.asset("assets/image/ic_more_app_bar.png")],
        backgroundColor: AppColor.colorBackground,
        title: const Text(
          "Profile",
          style: FontTextStyle.appBarText,
        ),
        centerTitle: true,
      ),
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverList(
              delegate: SliverChildListDelegate([
                Container(
                  padding: const EdgeInsets.only(bottom: 32),
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(70),
                        bottomLeft: Radius.circular(70)),
                    color: AppColor.colorBackground,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ClipOval(
                        child: SizedBox.fromSize(
                          size: const Size.fromRadius(45),
                          child: Image.network(
                              "https://img.cdn.vncdn.io/cinema/img/85633909811108353-1.jpg",
                              fit: BoxFit.cover),
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      const Text(
                        "Fauzian Ahmad",
                        style: FontTextStyle.fontTextWhite,
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      const Text(
                        "fauzianahmad04@gmail.com",
                        style: FontTextStyle.fontTextWhite38,
                      ),
                      const SizedBox(
                        height: 32,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: const [
                              Text(
                                "Followers",
                                style: FontTextStyle.fontTextWhite38,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                "129",
                                style: FontTextStyle.fontTextWhite,
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: const [
                              Text(
                                "Following",
                                style: FontTextStyle.fontTextWhite38,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                "238",
                                style: FontTextStyle.fontTextWhite,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ];
        },
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 32,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: const [
                          Image(
                            image: AssetImage("assets/image/ic_profile_add.png"),
                            fit: BoxFit.cover,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Find friend",
                            style: FontTextStyle.fontTextWhite,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: const [
                          Image(
                            image: AssetImage("assets/image/ic_share.png"),
                            fit: BoxFit.cover,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Share",
                            style: FontTextStyle.fontTextWhite,
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                  Container(
                    height: 16,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                        colors: <Color>[
                          AppColor.colorBackground.withOpacity(0.08),
                          AppColor.colorWhiteBorder.withOpacity(0.4)
                        ],
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(left: 32, top: 24),
                    child: Text(
                      "Mostly played",
                      style: FontTextStyle.appBarText,
                    ),
                  ),
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.only(left: 32, right: 32, top: 9),
                child: SizedBox(
                  height: 300,
                  child: ListViewItem(
                    image: _items,
                    icon: "assets/image/ic_more_1.png",
                    isCheckRadius: true,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
