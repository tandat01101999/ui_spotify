import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_1/core/app_color.dart';
import 'package:test_1/widget/home/tab_bar_home_widget.dart';
import 'package:test_1/widget/home/widget_home_list_view.dart';

import '../core/app_font_style.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>{
  List _items = [];
  List _singer = [];

  Future<void> readJson() async {
    final String response =
        await rootBundle.loadString('assets/json/category.json');
    final data = await json.decode(response);
    setState(() {
      _items = data["items"];
      _singer = data["singer"];
    });
  }

  @override
  void initState() {
    super.initState();
    readJson();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.colorBackgroundNew,
      appBar: AppBar(
        leading: Image.asset(
          "assets/image/ic_search.png",
        ),
        actions: [Image.asset("assets/image/ic_setting.png")],
        backgroundColor: AppColor.colorBackgroundNew,
        title: const Image(
          image: AssetImage("assets/image/ic_logo_home.png"),
          fit: BoxFit.cover,
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 20, bottom: 20),
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 32, left: 32),
                    child: Stack(
                      children: [
                        Positioned(
                          top: 30,
                          right: 0,
                          left: 0,
                          bottom: 0,
                          child: Container(
                            width: 364,
                            height: 128,
                            decoration: BoxDecoration(
                              color: AppColor.colorHomeYellow,
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.white54,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const [
                                    Text(
                                      "Popular",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.white),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      "Sisa Rasa",
                                      style: TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white),
                                    ),
                                    Text(
                                      "Mahalini",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.white),
                                    ),
                                  ],
                                ),
                              ),
                              const Image(
                                image: AssetImage("assets/image/ic_image.png"),
                                fit: BoxFit.cover,
                              ),
                              const Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  color: Colors.white54,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(left: 32),
                    child: Text(
                      "Today’s hits",
                      style: FontTextStyle.appBarText,
                    ),
                  ),
                  WidgetHomeListView(item: _items),
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: TabBarHomeWidget(item: _singer,),
            ),
          ],
        ),
      ),
    );
  }
}
