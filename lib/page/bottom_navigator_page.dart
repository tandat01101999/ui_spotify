
import 'package:flutter/material.dart';
import 'package:test_1/core/app_color.dart';
import 'package:test_1/page/history_page.dart';
import 'package:test_1/page/home_page.dart';
import 'package:test_1/page/playlist_page.dart';
import 'package:test_1/page/profile_page.dart';


class Home extends StatefulWidget {
  const Home({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {


  int currentTab = 0;
  final List<Widget> screens = [
    const HomePage(),
    const PlaylistPage(),
    const HistoryPage(),
    const ProfilePage(),
  ];
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = const HomePage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        bucket: bucket,
        child: currentScreen,
      ),
      floatingActionButtonLocation:
      FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          padding: const EdgeInsets.only(bottom: 45.0),
          child:  FloatingActionButton(
            backgroundColor: Colors.transparent,
            onPressed: () {},
              child: const Image(
                image: AssetImage("assets/image/ic_spotify.png"),
                fit: BoxFit.cover,
              ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: AppColor.colorBackground,
        child: SizedBox(
          height: 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            const HomePage();
                        currentTab = 0;
                      });
                    },
                    child: Column(
                      children: <Widget>[
                        currentTab == 0 ? Container(
                          width: 24,
                          height: 5,
                          decoration: BoxDecoration(
                              color: currentTab == 0 ? Colors.green : Colors.grey,
                            borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(100),bottomRight: Radius.circular(100))
                          ),
                          margin: const EdgeInsets.only(bottom: 10),
                        ):Container(
                          width: 24,
                          height: 5,
                          margin: const EdgeInsets.only(bottom: 10),
                        ),
                        Image.asset(currentTab == 0 ? "assets/image/ic_bottom_home_active.png": "assets/image/ic_bottom_home.png" ,fit: BoxFit.cover),
                        Text(
                          'Home',
                          style: TextStyle(
                            color: currentTab == 0 ? Colors.green : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            const PlaylistPage();
                        currentTab = 1;
                      });
                    },
                    child: Column(
                      children: <Widget>[
                        currentTab == 1 ? Container(
                          width: 24,
                          height: 5,
                          decoration: BoxDecoration(
                              color: currentTab == 1 ? Colors.green : Colors.grey,
                              borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(100),bottomRight: Radius.circular(100))
                          ),
                          margin: const EdgeInsets.only(bottom: 10),
                        ):Container(
                          width: 24,
                          height: 5,
                          margin: const EdgeInsets.only(bottom: 10),
                        ),
                        Image.asset("assets/image/ic_bottom_playlist.png",fit: BoxFit.cover,color: currentTab == 1 ? Colors.green : Colors.grey,),
                        Text(
                          'Playlist',
                          style: TextStyle(
                            color: currentTab == 1 ? Colors.green : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            const HistoryPage();
                        currentTab = 2;
                      });
                    },
                    child: Column(
                      children: <Widget>[
                        currentTab == 2 ? Container(
                          width: 24,
                          height: 5,
                          decoration: BoxDecoration(
                              color: currentTab == 2 ? Colors.green : Colors.grey,
                              borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(100),bottomRight: Radius.circular(100))
                          ),
                          margin: const EdgeInsets.only(bottom: 10),
                        ):Container(
                          width: 24,
                          height: 5,
                          margin: const EdgeInsets.only(bottom: 10),
                        ),
                        Image.asset("assets/image/ic_bottom_history.png",fit: BoxFit.cover,color: currentTab == 2 ? Colors.green : Colors.grey,),
                        Text(
                          'History',
                          style: TextStyle(
                            color: currentTab == 2 ? Colors.green : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            const ProfilePage();
                        currentTab = 3;
                      });
                    },
                    child: Column(
                      children: <Widget>[
                        currentTab == 3 ? Container(
                          width: 24,
                          height: 5,
                          decoration: BoxDecoration(
                              color: currentTab == 3 ? Colors.green : Colors.grey,
                              borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(100),bottomRight: Radius.circular(100))
                          ),
                          margin: const EdgeInsets.only(bottom: 10),
                        ):Container(
                          width: 24,
                          height: 5,
                          margin: const EdgeInsets.only(bottom: 10),
                        ),
                        Image.asset("assets/image/ic_bottom_profile.png",fit: BoxFit.cover,color: currentTab == 3 ? Colors.green : Colors.grey,),
                        Text(
                          'Profile',
                          style: TextStyle(
                            color: currentTab == 3 ? Colors.green : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}