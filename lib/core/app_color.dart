

import 'dart:ui';

class AppColor{
  static const colorBackground = Color(0xFF333333);
  static const colorBackgroundNew = Color(0xFF000000);
  static const colorWhiteBorder = Color(0xFFD9D9D9);
  static const colorHomeYellow = Color(0xFFD7BD1E);
}